---
title: "Graf"
date: 2018-08-08T19:58:13-07:00
categories: [beer, recipe]
tags: [cider, stout]
draft: false
---

Brewed August 8, 2018. Currently fermenting.

![Graf](/img/generic01.jpeg)

I have no recollection of how I stumbled across this style of beer but this is a
huge experiment.

# Grain bill

Weight | Type
--- | ---
3.5 lb | 2-row
1.0 lb | roasted barley
0.5 lb | smoked malt
0.5 lb | chocolate wheat
0.5 lb | crystal 120
**6.0 lb** |

# Hops/additions

Amount | Type | Time
--- | --- | ---
2oz | Centennial | 60 min
1oz | Cascade | 10 min
2 gal | Pasteurized apple juice | 0 min

# Fermentation

White Labs WLP090 San Diego Super Yeast @ 68F.

# Mash

* 6 lb grain to 2.7 gal mash = 1.78 qt/lb.
* Strike 166F -> 156 F rest for 60 min.
* Used 3.5 gal sparge.

Was a bit rushed to complete this and the [blanche oreiller][1] brew on the same day so
didn't measure actual mash temperature, mash pH, or even check for conversion
with iodine.

# Measurements

Somehow completely forgot to take a OG reading of this one... ಠ_ಠ

* ?.??? August 8
* 1.010 August 25

# Water adjustments

Adjustment | Mash | Sparge
--- | --- | ---
CaSO<sub>4</sub> | - | -
CaCl<sub>2</sub> | 2.3 g | 3.8 g
MgSO<sub>4</sub> | 2.0 g | 2.6 g
NaHCO<sub>3</sub>| 3.0 g | 3.9 g
lactic acid | -

# Water profile

Ca  | Mg  | Na  | Cl  | SO<sub>4</sub> | Cl:SO<sub>4</sub>
--- | --- | --- | --- | --- | ---
80  | 20  | 82  | 110 | 84  | 1.31

[1]: {{< ref "/blogs/blanche-oreiller" >}}
