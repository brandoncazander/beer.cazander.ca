---
title: "Nectarous Clone"
date: 2018-07-08T19:42:09-07:00
categories: [beer, recipe]
tags: [beer, clone, sour, dry hop]
draft: false
---

Brewed July 8, 2018. Bottled July 30. Finished August 24.

![Nectarous clone](/img/nectarous.jpg)

I'm such a huge fan of Nectarous by Four Winds, so this was my first earnest
attempt at cloning it. Basically this is a kettle sour that I dry-hopped.

I have always soured in the past using starters I made from kefir (Liberté
Mango), but this was the first time I skipped the starter completely and just
added the kefir directly to the kettle after cooling prior to souring.

The initial tasting after about a week of bottle conditioning was disappointing.
It was quite grassy: I forgot to record when I dry-hopped but I think the hops
were left for 5-7 days before bottling. Beyond that, the tartness was about
perfect for what I like.

By the time this was finished off in late August it was fast becoming one of my
favorites. I think I'll brew this again.

# Grain bill

Weight | Type
--- | ---
7.5 lb | pilsner
2.7 lb | wheat
0.6 lb | carapils
**10.8 lb** |

# Souring

After regular mash I boiled for 20 minutes to sanitize wort and then cooled to
100F and put kettle on stove for three days. I always pre-acidify my wort to a
pH of 4.5 before adding the lacto and souring, and this was no different. I
directly added two cups of Liberté Mango kefir to the kettle, wrapped in cling
wrap and used my wife's best duvet to keep the kettle temperature up.

The kettle was left on the stove for three days without any disturbances aside
from making sure the temperature didn't drop too far by periodically turning on
the stove on low until the temperature probe read 100F again.

After three days, the pH was measured at 3.3 and the wort was again boiled for
10 minutes to kill the lactobacillus. At this point, the whirlpool hops were
added.

# Hops/additions

Amount | Type | Time
--- | --- | ---
1oz | Citra | whirlpool
1oz | Cascade | whirlpool
1oz | Citra | dry-hop
1oz | Cascade | dry-hop

# Fermentation

White Labs WLP644 Saccharomyces "Bruxellensis" Trois @ ??F.

# Mash

* 10.8 lb grain to 4 gal mash = 1.48 qt/lb.
* Strike 166F -> 154F rest for 60 min.
* 5 gal sparge.
* 5.58 pH target mash, 5.6 pH measured.

# Measurements

* 1.046 July 8 (pre-boil)
* 1.040 July 11 (post-boil)
* 1.010 July 30

# Bottling

* Bottled July 30 with 6.1 oz bottling sugar.

# Water adjustments

Adjustment | Mash
--- | ---
CaSO<sub>4</sub> | 3.0 g
CaCl<sub>2</sub> | 5.0 g
MgSO<sub>4</sub> | 3.0 g
NaHCO<sub>3</sub>| -
lactic acid | 2 mL

# Water profile

Ca  | Mg  | Na  | Cl  | SO<sub>4</sub> | Cl:SO<sub>4</sub>
--- | --- | --- | --- | --- | ---
79  | 10  | 2   | 72  | 91  | 0.79
