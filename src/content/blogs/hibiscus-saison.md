---
title: "Hibiscus Saison"
date: 2018-08-08T19:14:11-07:00
categories: [beer, recipe]
tags: [beer, hibiscus, saison]
draft: false
---

Brewed July 8, 2018. Bottled July 30. Finished August 24.

![Hibiscus saison](/img/hibiscus_saison.jpg)

I ordered a bunch of dried hibiscus on Amazon to make this saison, which was
initially very promising but something went wrong and I haven't been able to
completely nail down a cause.

At bottling time and shortly after it was delicious, but I found many bottles
had a disturbing metallic taste initially. I have a list of possible culprits
but I'm going to brew it again to verify:

* Some bottles weren't fully capped and would leak if placed on their side.
* The hibiscus leaves were boiled in water prior to dry-hopping with them. Maybe
  boiling for 5 minutes was enough to bring out bad flavors?
* Could this batch have been infected? The FG was quite low compared to anything
  I have made before, but didn't over-carbonate.

With that said, this is a promising recipe. The bottles that didn't have the
metallic initial taste were splendid.

# Hops/additions

Amount | Type | Time
--- | --- | ---
2 oz | Styrion | 60 min
6.0 g | Hibiscus | 20 min
1/2 | Whirlfloc tablet | 20 min
5 oz | boiled hibiscus | 3 days

# Measurements

* 1.022 July 8 (pre-boil)
* 1.034 July 8 (post-boil)
* 1.004 July 30

# Bottling

* Bottled July 30 with 6.4 oz bottling sugar.

# Water adjustments

Adjustment | Mash
--- | ---
CaSO<sub>4</sub> | 3.0 g
CaCl<sub>2</sub> | 5.17 g
MgSO<sub>4</sub> | 3.0 g<sup>*</sup>
NaHCO<sub>3</sub>| -
lactic acid | 2 mL

_<sup>*</sup> totally forgot this one when prepping_

# Water profile

Ca  | Mg  | Na  | Cl  | SO<sub>4</sub> | Cl:SO<sub>4</sub>
--- | --- | --- | --- | --- | ---
84  | 11  | 2   | 79  | 99  | 0.80
