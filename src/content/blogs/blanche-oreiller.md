---
title: "Blanche Oreiller"
date: 2018-08-25T19:58:06-07:00
categories: [beer, recipe]
tags: [witbier]
draft: false
---

Brewed August 8, 2018. Currently fermenting.

![Blanche Oreiller](/img/generic03.jpeg)

At this point I have roughly 15 batches under my belt: this recipe was my start
to try and improve two things about my brewing:

1. Find a good, basic recipe that I can make really repeatable and nail down my
   process.
2. Brew a crowd-pleasing beer that everyone in my circles will enjoy. As much as
   I love sour beers and could never drink anything else again, the best part of
   brewing is sharing my creations and non-sour beer just has a larger crowd.

# Grain bill

Weight | Type | Percentage
--- | --- | ---
3.2 lb | wheat | 40%
3.5 lb | pilsner | 44%
0.8 lb | flaked oats | 10%
0.2 lb | munich | 2%
0.4 lb | rice hulls | 4%
**8.1 lb** | |

# Hops/additions

Amount | Type | Time
--- | --- | ---
1 oz | Hallertau | 60 min
1.5 oz | Citrus zest | 5 min
1.38 g | Crushed coriander | 5 min
1 g | Hibiscus | 5 min

# Fermentation

2 x SafAle T58 @ 68F. Was meant to raise temperature to 72F over period of two
weeks but we left on holidays shortly after brewing so I raised it to 72F over
the first four days of fermentation. This is where the ITC-306T ​would make a
welcome upgrade.

# Mash

* 8.1 lb grain to 3.6 gal mash = 1.78 qt/lb.
* Strike 164F -> 154F rest for 60 min.
* Used 4.6 gal sparge.

Was a bit rushed to complete this and the [graf][1] brew on the same day so
didn't measure actual mash temperature, mash pH, or even check for conversion
with iodine.

# Measurements

* 1.028 August 8
* 1.009 August 25

# Water adjustments

Adjustment | Mash | Sparge
--- | --- | ---
CaSO<sub>4</sub> | 1 g | 1.3 g
CaCl<sub>2</sub> | 3 g | 3.8 g
MgSO<sub>4</sub> | 2 g | 2.6 g
NaHCO<sub>3</sub>| 1.5 g | 1.9 g
lactic acid | 4 mL

# Water profile

Ca | Mg | Na | Cl | SO<sub>4</sub> | Cl:SO<sub>4</sub>
--- | --- | --- | --- | --- | ---
95 | 15 | 32 | 107| 106| 1.02

[1]: {{< ref "/blogs/graf" >}}
