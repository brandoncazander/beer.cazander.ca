---
title: "About"
date: 2018-08-25T20:45:16-07:00
draft: false
---

I started homebrewing in August 2017 after picking up a starter kit at my
favorite homebrew shop, [GoldSteam Homebrew Supplies][1] in Chilliwack.

If you have any comments about this site or tips/recipes for me, please send
them to <a href="mailto:brandon+beer@cazander.ca">root@localhost</a>.

[1]: https://goldsteam.com/

